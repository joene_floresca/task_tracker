@extends('app_ext')
@section('content')
<style>
.gray-bg {
    background-color: #2196F3;
}
</style>
<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-success">Notes</h4>
        </div>
        <div class="modal-body">
          <p>If you need help on how to use the system, please click the Question Mark icon on the upper-left part of this page.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>

            <!-- <h1 class="logo-name">IN+</h1> -->
            <center><img class="img-circle" src="{{asset('img/task_logo.png')}}" height="150" width="150"></center>
            <img id="help-img" src="{{asset('img/help.png')}}" >
            <a id="help" href="{{ url('help') }}"><span class="glyphicon glyphicon-question-sign"></span></a>
        

        </div>
        <h3 class="white">Welcome to QDF Task Tracker</h3>
        @if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
              @if(Session::has('alert-' . $msg))
              <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
              @endif
            @endforeach
        </div>
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <input type="text" class="form-control" name="id_number" placeholder="ID Number" required="">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="password" placeholder="Password" required="">
            </div>

            <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

            <!-- <a href="{{ url('/password/email') }}"><small>Forgot password?</small></a> -->
            <p class="text-muted text-center"><small class="white">Do not have an account?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="{{ url('/auth/register') }}">Create an account</a>
        </form>
        <p class="m-t white"> <small><strong>Copyright</strong> Quinn Data Facilities &copy; <?php echo date("Y") ?></small> </p>
    </div>
</div>

@endsection

@section('login')
<script type="text/javascript">
    // $(window).load(function(){
    //     $('#myModal').modal('show');
    // });
</script>      
@endsection