@extends('app_ext')
@section('content')

<div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-success">
                        <div class="panel-heading">Help Page</div>
                        <div class="panel-body">
                            <h4>Help Page</h4>

                                            <p>QDF Tasks Tracker - is a web-based application to track daily tasks of employees which can be viewed by supervisors. You can add your tasks, assign it as daily or additional task and mark it as pending or done.</p>
                                            <p> Everyday, before leaving, you will generate and submit your task report to the supervisor.</p>

                                            <h4>Process</h4>

                                            <ul>
                                                <li>Register for your account <a href="http://192.168.3.164/task_tracker/public/index.php/auth/login">http://192.168.3.164/task_tracker/public/index.php/auth/login</a></li>
                                                <li>Login <a href="http://192.168.3.164/task_tracker/public/index.php/auth/login">http://192.168.3.164/task_tracker/public/index.php/auth/login</a></li>
                                                <li>Create your tasks http://192.168.3.164/task_tracker/public/index.php/tasks/create then tag it as Daily or not.</li>
                                                <li>Upon login you will be directed to your tasks list page which has three sections
                                                        <ul>
                                                            <li>My Daily Tasks - Which contains all your tasks tagged as your daily tasks. All created tasks will be marked as pending and can be changed by clicking 'Details' button in the 'Action' column and a pop-up box will appear allowing you to change the status of the task.</li>
                                                            <li>My Additional Tasks - This is your additional tasks either added by you or added by supervisor and assigned to you. Status can also be changed same with daily tasks.</li>
                                                            <li>Generate Report - This where you will submit your tasks to the supervisor. When you click the 'Generate' button it will get all your daily tasks and all additional tasks that is created within the last 12 hours. Once you click 'Send Report' and a pop-up message will appear that the email has been sent. </li>
                                                        </ul>   
                                                </li>
                                            </ul>

                                            <p>It will be auto-populated in the Generate Report table as an overview for you to see the report that you will send to the supervisor. Once generated. You can click the 'Send Report' button and the system will send your task report to the supervisor.</p>
                                            
                                            <h4>FAQ's</h4>

                                            <ul>
                                                <li>Why there are separate task type (Daily and Additional Task)?
                                                    <ul>
                                                        <li>
                                                            Daily tasks are the one that you are doing everyday, you only need to add/encode this once.
                                                            When you click the generate button, all your daily tasks will be included because that is tagged as daily 
                                                            regardless the date you added it. If you added a daily task 2 days ago then it will be included in the generate report today.
                                                            For daily tasks, the system will ignore the start and end <b><i>DATE</i></b> since it is daily <b>BUT</b> the system will
                                                            still calulate the <b><i>TIME</i></b> for total hours computation. You are free to update the daily task time frame if needed
                                                            by clicking the 'Details' button and saving it.
                                                        </li>
                                                        <li>
                                                            Additional task are tasks that you are not doing everyday. e.g Task added by Supervisor. Once you click the Generate Report button, 
                                                            all Additional Tasks that is added within the last 12 hours from the current time will be added in the generate report. <b><i>Example:</i></b>
                                                            You click the generate button on May 06, 2016 at 10PM. What will happen is all your daily tasks (regardless of date) and all additional tasks created from May 06, 2016 at 10AM
                                                            to May 06, 2016 at 10PM will be added in the report. That is 12 hours so everyone who is extending their shifts will have no issue.
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>Why is it that the time frame for getting the additional tasks is within the last 12 hours?
                                                    <ul>
                                                        <li>Usually a typical work hours is 8, but the system will set it as 12 for people who are extending work hours.</li>
                                                    </ul>
                                                </li>
                                                <li>Can I delete my added task?
                                                    <ul>
                                                        <li>No, once you added a task you can only edit it to make corrections.</li>
                                                    </ul>
                                                </li>
                                                <li>How many tasks I can add per day?
                                                    <ul>
                                                        <li>There's no limit for adding tasks, just assign it accordingly (Either Daily or Additional)</li>
                                                    </ul>
                                                </li>
                                                <li>Can I generate and send a report if some of my task is still pending?
                                                    <ul>
                                                        <li>Yes, some tasks requires more than 1 day to finish.</li>
                                                    </ul>
                                                </li>
                                                <li>How will the system send the report to the supervisor?
                                                    <ul>
                                                        <li>Via Email.</li>
                                                    </ul>
                                                </li>
                                            </ul>  

                                            <h4>Contact Person</h4>

                                            <p>If you discover a glitch or error in the system, you can report it to <a href="mailto:mis@qdf-phils.com" target="_top">mis@qdf-phils.com</a></p>

                                            <h4>How to report error or glitches</h4>

                                            <ul>
                                                <li>Screenshot the error message (if any)</li>
                                                <li>Describe the process that you are doing when the error occured</li>
                                                <li>Email it to mis@qdf-phils.com</li>
                                            </ul>
                        </div>
                    </div>
                    
                </div>             
            </div>

        </div> @endsection



