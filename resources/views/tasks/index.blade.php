@extends('app')
@section('content')
<style>
th { font-size: 12px; }
td { font-size: 11px; }
/* Start by setting display:none to make this hidden.
   Then we position it in relation to the viewport window
   with position:fixed. Width, height, top and left speak
   for themselves. Background we set to 80% white with
   our animation centered, and no-repeating */
.load-page {
    display:    none;
    position:   fixed;
    z-index:    1000;
    top:        0;
    left:       0;
    height:     100%;
    width:      100%;
    background: rgba( 255, 255, 255, .8 ) 
                url('http://i.stack.imgur.com/FhHRx.gif') 
                50% 50% 
                no-repeat;
}

/* When the body has the loading class, we turn
   the scrollbar off with overflow:hidden */
body.loading {
    overflow: hidden;   
}

/* Anytime the body has the loading class, our
   modal element will be visible */
body.loading .load-page {
    display: block;
}
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>My Tasks</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('home') }}">Home</a>
            </li>
            <li class="active">
                <strong>Tasks List</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
            @if(Auth::check() && Auth::user()->access_level != 1)
            <div class="row A">
                <div class="col-lg-12">

                    <div class="panel panel-primary">
                        <div class="panel-heading">My Daily Tasks</div>
                        <div class="panel-body">
                            <table id="MyDailyTasks" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Description</th>
                                        <th>Start</th>
                                        <th>End</th>
                                        <th>Added By</th>
                                        <th>Assigned to</th>
                                        <th>Date Created</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- <div class="ibox-title">
                        <h5>My Daily Tasks</small></h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <table id="MyDailyTasks" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Description</th>
                                    <th>Start</th>
                                    <th>End</th>
                                    <th>Added By</th>
                                    <th>Assigned to</th>
                                    <th>Date Created</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div> -->
                </div>
            </div>
            @endif

            <div class="row B">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">

                        <!-- <div class="ibox-title">
                            @if(Auth::check() && Auth::user()->access_level != 1)
                                <h5>My Additional Tasks</small></h5>
                            @else
                                 <h5>All Tasks</small></h5>   
                            @endif
                            
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div> -->
                        
                        <!-- Modal Update -->
                        <div id="myModal" class="modal fade" role="dialog">
                          <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                          <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">My Task</h4>
                                          </div>

                                          <div class="modal-body">
                                                    <div class="form-horizontal"  role="form">
                                                        <fieldset>
                                                                <div class="form-group">
                                                                    <label  class="col-sm-2 control-label">ID</label>
                                                                    <div class="col-lg-10">
                                                                        <input type="text" class="form-control" id="task_id" name="task_id" readonly="readonly">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label  class="col-sm-2 control-label">Description</label>
                                                                    <div class="col-lg-10">
                                                                        <input type="text" class="form-control" id="task_description" name="task_description">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label  class="col-sm-2 control-label">Start</label>
                                                                    <div class="col-lg-10">
                                                                        <input type="text" class="form-control datetimepicker" id="task_start" name="task_start" >
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label  class="col-sm-2 control-label">End</label>
                                                                    <div class="col-lg-10">
                                                                        <input type="text" class="form-control datetimepicker" id="task_end" name="task_end" >
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="form-group">
                                                                    <label  class="col-sm-2 control-label">Added By</label>
                                                                    <div class="col-lg-10">
                                                                        <input type="text" class="form-control" id="task_added" name="task_added" readonly="readonly">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group" style="">
                                                                    <label  class="col-sm-2 control-label">Is task daily?</label>
                                                                    <div class="col-lg-10">
                                                                        <select name="is_daily" id="is_daily" class="form-control">
                                                                            <option value=""></option>
                                                                            <option value="1">Yes</option>
                                                                            <option value="0">No</option>
                                                                        </select>
                                                                    </div>
                                                              </div>
                                                                <div class="form-group">
                                                                    <label  class="col-sm-2 control-label">Status</label>
                                                                    <div class="col-lg-10">
                                                                        <select class="form-control" name="task_status" id="task_status">
                                                                            <option value=""></option>
                                                                            <option value="Pending">Pending</option>
                                                                            <option value="Done">Done</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                        </fieldset>
                                                    </div><!-- form-horizontal -->
                                          </div> <!-- modal-body -->

                                  <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-success" data-dismiss="modal" id="updateData">Save</button>
                                  </div>

                                </div> <!-- modal-content -->

                          </div> <!-- -->
                        </div> <!-- -->

                        <!-- Modal Help-->
                        <div id="myModalHelp" class="modal fade" role="dialog">
                          <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                          <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">QDF - Task Tracker</h4>
                                          </div>

                                          <div class="modal-body">
                                            <h4>Help Page</h4>

                                            <p>QDF Tasks Tracker - is a web-based application to track daily tasks of employees which can be viewed by supervisors. You can add your tasks, assign it as daily or additional task and mark it as pending or done.</p>
                                            <p> Everyday, before leaving, you will generate and submit your task report to the supervisor.</p>

                                            <h4>Process</h4>

                                            <ul>
                                                <li>Register for your account <a href="http://192.168.3.164/task_tracker/public/index.php/auth/login">http://192.168.3.164/task_tracker/public/index.php/auth/login</a></li>
                                                <li>Login <a href="http://192.168.3.164/task_tracker/public/index.php/auth/login">http://192.168.3.164/task_tracker/public/index.php/auth/login</a></li>
                                                <li>Create your tasks http://192.168.3.164/task_tracker/public/index.php/tasks/create then tag it as Daily or not.</li>
                                                <li>Upon login you will be directed to your tasks list page which has three sections
                                                        <ul>
                                                            <li>My Daily Tasks - Which contains all your tasks tagged as your daily tasks. All created tasks will be marked as pending and can be changed by clicking 'Details' button in the 'Action' column and a pop-up box will appear allowing you to change the status of the task.</li>
                                                            <li>My Additional Tasks - This is your additional tasks either added by you or added by supervisor and assigned to you. Status can also be changed same with daily tasks.</li>
                                                            <li>Generate Report - This where you will submit your tasks to the supervisor. When you click the 'Generate' button it will get all your daily tasks and all additional tasks that is created within the last 12 hours. Once you click 'Send Report' and a pop-up message will appear that the email has been sent. </li>
                                                        </ul>   
                                                </li>
                                            </ul>

                                            <p>It will be auto-populated in the Generate Report table as an overview for you to see the report that you will send to the supervisor. Once generated. You can click the 'Send Report' button and the system will send your task report to the supervisor.</p>
                                            
                                            <h4>FAQ's</h4>

                                            <ul>
                                                <li>Why there are separate task type (Daily and Additional Task)?
                                                    <ul>
                                                        <li>
                                                            Daily tasks are the one that you are doing everyday, you only need to add/encode this once.
                                                            When you click the generate button, all your daily tasks will be included because that is tagged as daily 
                                                            regardless the date you added it. If you added a daily task 2 days ago then it will be included in the generate report today.
                                                            For daily tasks, the system will ignore the start and end <b><i>DATE</i></b> since it is daily <b>BUT</b> the system will
                                                            still calulate the <b><i>TIME</i></b> for total hours computation. You are free to update the daily task time frame if needed
                                                            by clicking the 'Details' button and saving it.
                                                        </li>
                                                        <li>
                                                            Additional task are tasks that you are not doing everyday. e.g Task added by Supervisor. Once you click the Generate Report button, 
                                                            all Additional Tasks that is added within the last 12 hours from the current time will be added in the generate report. <b><i>Example:</i></b>
                                                            You click the generate button on May 06, 2016 at 10PM. What will happen is all your daily tasks (regardless of date) and all additional tasks created from May 06, 2016 at 10AM
                                                            to May 06, 2016 at 10PM will be added in the report. That is 12 hours so everyone who is extending their shifts will have no issue.
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>Why is it that the time frame for getting the additional tasks is within the last 12 hours?
                                                    <ul>
                                                        <li>Usually a typical work hours is 8, but the system will set it as 12 for people who are extending work hours.</li>
                                                    </ul>
                                                </li>
                                                <li>Can I delete my added task?
                                                    <ul>
                                                        <li>No, once you added a task you can only edit it to make corrections.</li>
                                                    </ul>
                                                </li>
                                                <li>How many tasks I can add per day?
                                                    <ul>
                                                        <li>There's no limit for adding tasks, just assign it accordingly (Either Daily or Additional)</li>
                                                    </ul>
                                                </li>
                                                <li>Can I generate and send a report if some of my task is still pending?
                                                    <ul>
                                                        <li>Yes, some tasks requires more than 1 day to finish.</li>
                                                    </ul>
                                                </li>
                                                <li>How will the system send the report to the supervisor?
                                                    <ul>
                                                        <li>Via Email.</li>
                                                    </ul>
                                                </li>
                                            </ul>  

                                            <h4>Contact Person</h4>

                                            <p>If you discover a glitch or error in the system, you can report it to <a href="mailto:mis@qdf-phils.com" target="_top">mis@qdf-phils.com</a></p>

                                            <h4>How to report error or glitches</h4>

                                            <ul>
                                                <li>Screenshot the error message (if any)</li>
                                                <li>Describe the process that you are doing when the error occured</li>
                                                <li>Email it to mis@qdf-phils.com</li>
                                            </ul>
                                                    
                                          </div> <!-- modal-body -->

                                  <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  </div>

                                </div> <!-- modal-content -->

                          </div> <!-- -->
                        </div> <!-- -->

                        @if(Auth::check() && Auth::user()->access_level != 1)
                        <div class="panel panel-success">
                            <div class="panel-heading">My Additional Tasks</div>
                            <div class="panel-body">
                                <table id="MyTasks" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Description</th>
                                        <th>Start</th>
                                        <th>End</th>
                                        <th>Added By</th>
                                        <th>Assigned to</th>
                                        <th>Date Created</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                </tbody>
                            </table>
                            </div>
                        </div>

                        <!-- <div class="ibox-content">
                            <table id="MyTasks" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Description</th>
                                        <th>Start</th>
                                        <th>End</th>
                                        <th>Added By</th>
                                        <th>Assigned to</th>
                                        <th>Date Created</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                </tbody>
                            </table>
                        </div> -->
                        @endif

                        @if(Auth::check() && Auth::user()->access_level == 1)
                        <div class="ibox-content">
                            <table id="MyTasksAdmin" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Description</th>
                                        <th>Start</th>
                                        <th>End</th>
                                        <th>Added By</th>
                                        <th>Assigned to</th>
                                        <th>Date Created</th>
                                        <th>Task Type</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>

                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        @endif

                    </div>
                </div>
            </div>
            @if(Auth::check() && Auth::user()->access_level != 1)
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-warning">
                        <div class="panel-heading">Generate Report</div>
                        <div class="panel-body">
                            <table id="SendTask" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Description</th>
                                        <th>Start</th>
                                        <th>End</th>
                                        <th>Hours</th>
                                        <th>Added By</th>
                                        <th>Assigned to</th>
                                        <th>Date Created</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>

                                <tbody>
                                </tbody>
                                 <tfoot>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th colspan="2">Total Hours</th>
                                        <th id="totalhours"></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                            <button type="button" class="btn btn-success" id="generateReport">Generate</button>
                            <button type="button" class="btn btn-primary" id="sendReport">Send Report</button>
                        </div>
                    </div>

                    <!-- <div class="ibox-title">
                        <h5>Generate Report</small></h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div> -->

                    <!-- <div class="ibox-content">
                        <table id="SendTask" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Description</th>
                                    <th>Start</th>
                                    <th>End</th>
                                    <th>Hours</th>
                                    <th>Added By</th>
                                    <th>Assigned to</th>
                                    <th>Date Created</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                </tr>
                            </thead>

                            <tbody>
                            </tbody>
                             <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th colspan="2">Total Hours</th>
                                    <th id="totalhours"></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                        <button type="button" class="btn btn-success" id="generateReport">Generate</button>
                        <button type="button" class="btn btn-primary" id="sendReport">Send Report</button>
                    </div> -->

                </div>
            </div>
            @endif

</div>
<div class="load-page"><!-- Place at bottom of page --></div>
@endsection
@section('tasks-index')
<script>
$(document).ready(function(){
    $('#MyDailyTasks').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'job-index',
        "order": [[ 0, "desc" ]],
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
        },
        ajax: 'daily-tasks-list',
         "createdRow": function ( row, data, index ) {
            if (data.status == "Pending") {
                $('td', row).eq(7).addClass('text-warning');
            }
            else
            {
                $('td', row).eq(7).addClass('text-success');
            }
        },
        columns: [
            {data: 'id', name: 'tasks.id'},
            {data: 'task_description', name: 'tasks.task_description'},
            {data: 'start_timestamp', name: 'tasks.start_timestamp'},
            {data: 'end_timestamp', name: 'tasks.end_timestamp'},
            {data: 'name', name: 'users.name'},
            {data: 'assign', name: 'x.name'},
            {data: 'created_at', name: 'tasks.created_at'},
            {data: 'status', name: 'tasks.status'},
            {data: 'action', name: 'action'}
        ]
    });



    $('#myModal').on('show.bs.modal', function (e) {
        var uniqueId = $(e.relatedTarget).data('id');
        //Ajax Method
        $.ajax({
            type : 'get',
            url : 'tasks/'+uniqueId, //Here you will fetch records 
            success : function(response){
                $('.modal-title').html('Task ID: ' + uniqueId);
                $('#task_id').val(uniqueId);
                $('#task_description').val(response.task_description);
                $('#task_start').val(response.start_timestamp);
                $('#task_end').val(response.end_timestamp);
                $('#task_custom').val(response.fixed_timestamp);
                $('#task_added').val(response.name);
                $('#is_daily').val(response.is_daily);
                $('#task_status').val(response.status);
            }
        });
    });

    $('#updateData').on('click', function() {
        var id = $("#task_id").val();
        var status  = $('#task_status').val();
        var desc    = $('#task_description').val();
        var start   = $('#task_start').val();
        var end     = $('#task_end').val();
        var is_daily = $('#is_daily').val();
        $.ajax({
            type : 'put',
            url : 'tasks/'+id, //Here you will fetch records 
            data: 
            {
                'id': id, 
                'status': status,
                'desc': desc,
                'start': start,
                'end': end,
                'is_daily': is_daily,
            },
            success : function(response){
                alert(response);
                location.reload(true);
            }
        });
    });

    $("#generateReport").click(function() {
        if($.fn.dataTable.isDataTable('#SendTask')) 
        {
            t.destroy();
            t = $('#SendTask').DataTable({
                "order": [[ 0, "desc" ]],
                "dom": 'T<"clear">lfrtip',
                "aLengthMenu": [
                [50, 80, 100, -1],
                [50, 80, 100, "All"]
                ],
                "tableTools": {
                    "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
                }
            });

            t.clear().draw();

            $.ajax({
                type : 'get',
                url : 'generate-report', //Here you will fetch records 
                success : function(result){
                    var myObj = $.parseJSON(result);
                    var totalhours = 0;
                    var totalmins = 0;
                    $.each(myObj[0], function(key,value) {
                        var date_start = new Date(value.start_timestamp);
                        var date_end = new Date(value.end_timestamp);
                        var milisec = Math.abs(date_end - date_start);
                        var seconds = milisec / 1000;
                        var hours = parseFloat( seconds / 3600 );
                        totalhours += hours;

                        var isdaily = '';
                        if(value.is_daily == 0)
                        {
                            isdaily = 'Added'
                        }
                        else
                        {
                            isdaily = 'Daily'
                        }

                        t.row.add( [
                            value.id,
                            value.task_description,
                            value.start_timestamp,
                            value.end_timestamp,
                            hours.toFixed(2)+" hr(s)",
                            value.name,
                            value.assign,
                            value.created_at,
                            isdaily,
                            value.status
                        ] ).draw();
                    });

                    $.each(myObj[1], function(key,value) {
                        var date_start = new Date(value.start_timestamp);
                        var date_end = new Date(value.end_timestamp);
                        var milisec = Math.abs(date_end - date_start);
                        var seconds = milisec / 1000;
                        var hours = parseFloat( seconds / 3600 );
                        totalhours += hours;

                        var isdaily = '';
                        if(value.is_daily == 0)
                        {
                            isdaily = 'Added'
                        }
                        else
                        {
                            isdaily = 'Daily'
                        }

                        t.row.add( [
                            value.id,
                            value.task_description,
                            value.start_timestamp,
                            value.end_timestamp,
                            hours.toFixed(2)+" hr(s)",
                            value.name,
                            value.assign,
                            value.created_at,
                            isdaily,
                            value.status
                        ] ).draw();
                    });
                    
                    var final_compute = Math.ceil(totalhours.toFixed(2));
                    $('#totalhours').text(final_compute);
                    alert("Tasks from the last 12 hours generated.");
                }
            });
        }
        else
        {
            t = $('#SendTask').DataTable({
                "order": [[ 0, "desc" ]],
                "dom": 'T<"clear">lfrtip',
                "aLengthMenu": [
                [50, 80, 100, -1],
                [50, 80, 100, "All"]
                ],
                "tableTools": {
                    "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
                }
            });

            $.ajax({
                type : 'get',
                url : 'generate-report', //Here you will fetch records 
                success : function(result){
                    var myObj = $.parseJSON(result);
                    var totalhours = 0;
                    var totalmins = 0;
                    $.each(myObj[0], function(key,value) {
                        var date_start = new Date(value.start_timestamp.replace(/\ /g, 'T'));
                        var date_end = new Date(value.end_timestamp.replace(/\ /g, 'T'));
                        var milisec = Math.abs(date_end - date_start);
                        var seconds = milisec / 1000;
                        var hours = parseFloat( seconds / 3600 );
                        totalhours += hours;

                        var isdaily = '';
                        if(value.is_daily == 0)
                        {
                            isdaily = 'Added'
                        }
                        else
                        {
                            isdaily = 'Daily'
                        }

                        t.row.add( [
                            value.id,
                            value.task_description,
                            value.start_timestamp,
                            value.end_timestamp,
                            hours.toFixed(2)+" hr(s)",
                            value.name,
                            value.assign,
                            value.created_at,
                            isdaily,
                            value.status
                        ] ).draw();
                    });

                    $.each(myObj[1], function(key,value) {
                        var date_start = new Date(value.start_timestamp.replace(/\ /g, 'T'));
                        var date_end = new Date(value.end_timestamp.replace(/\ /g, 'T'));
                        var milisec = Math.abs(date_end - date_start);
                        var seconds = milisec / 1000;
                        var hours = parseFloat( seconds / 3600 );
                        totalhours += hours;

                        var isdaily = '';
                        if(value.is_daily == 0)
                        {
                            isdaily = 'Added'
                        }
                        else
                        {
                            isdaily = 'Daily'
                        }

                        t.row.add( [
                            value.id,
                            value.task_description,
                            value.start_timestamp,
                            value.end_timestamp,
                            hours.toFixed(2)+" hr(s)",
                            value.name,
                            value.assign,
                            value.created_at,
                            isdaily,
                            value.status
                        ] ).draw();
                    });
                    
                    var final_compute = Math.ceil(totalhours.toFixed(2));
                    $('#totalhours').text(final_compute);
                    alert("Tasks from the last 12 hours generated.");
                }
            });


        }  


      // $.ajax({
      //       type : 'get',
      //       url : 'generate-report', //Here you will fetch records 
      //       success : function(result){
      //           var myObj = $.parseJSON(result);
      //           var t = $('#SendTask').DataTable({
      //               "order": [[ 0, "desc" ]],
      //               "dom": 'T<"clear">lfrtip',
      //               "aLengthMenu": [
      //               [50, 80, 100, -1],
      //               [50, 80, 100, "All"]
      //               ],
      //               "tableTools": {
      //                   "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
      //               }
      //           });

      //           var totalhours = 0;
      //           var totalmins = 0;
      //           $.each(myObj[0], function(key,value) {
      //               var date_start = new Date(value.start_timestamp);
      //               var date_end = new Date(value.end_timestamp);
      //               var milisec = Math.abs(date_end - date_start);
      //               var seconds = milisec / 1000;
      //               var hours = parseFloat( seconds / 3600 );
      //               totalhours += hours;

      //               var isdaily = '';
      //               if(value.is_daily == 0)
      //               {
      //                   isdaily = 'Added'
      //               }
      //               else
      //               {
      //                   isdaily = 'Daily'
      //               }

      //               t.row.add( [
      //                   value.id,
      //                   value.task_description,
      //                   value.start_timestamp,
      //                   value.end_timestamp,
      //                   hours.toFixed(2)+" hr(s)",
      //                   value.name,
      //                   value.assign,
      //                   value.created_at,
      //                   isdaily,
      //                   value.status
      //               ] ).draw();
      //           });

      //           $.each(myObj[1], function(key,value) {
      //               var date_start = new Date(value.start_timestamp);
      //               var date_end = new Date(value.end_timestamp);
      //               var milisec = Math.abs(date_end - date_start);
      //               var seconds = milisec / 1000;
      //               var hours = parseFloat( seconds / 3600 );
      //               totalhours += hours;

      //               var isdaily = '';
      //               if(value.is_daily == 0)
      //               {
      //                   isdaily = 'Added'
      //               }
      //               else
      //               {
      //                   isdaily = 'Daily'
      //               }

      //               t.row.add( [
      //                   value.id,
      //                   value.task_description,
      //                   value.start_timestamp,
      //                   value.end_timestamp,
      //                   hours.toFixed(2)+" hr(s)",
      //                   value.name,
      //                   value.assign,
      //                   value.created_at,
      //                   isdaily,
      //                   value.status
      //               ] ).draw();
      //           });
                
      //           var final_compute = Math.ceil(totalhours.toFixed(2));
      //           $('#totalhours').text(final_compute);
      //           alert("Tasks from the last 12 hours generated.");
      //       }
      //   });
    });



    $("#sendReport").click(function() {
        $body = $("body");
        jQuery.ajaxSetup({
          beforeSend: function() {
           $body.addClass("loading");
          },
          complete: function(){
            alert("Email sent.");
             $body.removeClass("loading");
          },
          success: function() {}
        });

        var table = $('#SendTask').prop('outerHTML');
        $.ajax({
            type : 'post',
            url : 'generate-email', //Here you will fetch records 
            data: {'body': table},
            success : function(result){
                //alert("Email Sent.");
            }
        });
    });
   
}); 
</script> 

@if(Auth::check() && Auth::user()->access_level != 1)
<script>
$(document).ready(function(){
    $('#MyTasks').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'tasks-list',
        "order": [[ 0, "desc" ]],
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
        },
         "createdRow": function ( row, data, index ) {
            if (data.status == "Pending") {
                $('td', row).eq(7).addClass('text-warning');
            }
            else
            {
                $('td', row).eq(7).addClass('text-success');
            }
        },
        columns: [
            {data: 'id', name: 'tasks.id'},
            {data: 'task_description', name: 'tasks.task_description'},
            {data: 'start_timestamp', name: 'tasks.start_timestamp'},
            {data: 'end_timestamp', name: 'tasks.end_timestamp'},
            {data: 'name', name: 'users.name'},
            {data: 'assign', name: 'x.name'},
            {data: 'created_at', name: 'tasks.created_at'},
            {data: 'status', name: 'tasks.status'},
            {data: 'action', name: 'action'}
        ]
    });
});
</script>
@endif
<script>
$(document).ready(function(){
    $('#MyTasksAdmin').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'job-index',
        "order": [[ 0, "desc" ]],
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
        },
        ajax: 'tasks-list-admin',
         "createdRow": function ( row, data, index ) {
            if (data.status == 1) {
                $('td', row).eq(7).addClass('text-success').text('Daily');
            }
            else
            {
                $('td', row).eq(7).addClass('text-warning').text('Added');
            }
        },
        columns: [
            {data: 'id', name: 'tasks.id'},
            {data: 'task_description', name: 'tasks.task_description'},
            {data: 'start_timestamp', name: 'tasks.start_timestamp'},
            {data: 'end_timestamp', name: 'tasks.end_timestamp'},
            {data: 'name', name: 'users.name'},
            {data: 'assign', name: 'x.name'},
            {data: 'created_at', name: 'tasks.created_at'},
            {data: 'is_daily', name: 'is_daily'},
            {data: 'status', name: 'tasks.status'}
            
        ]
    });
   
}); 
</script> 
@endsection
